local variants = {
	main = {
		base = '#191724',
		surface = '#1f1d2e',
		overlay = '#26233a',
		muted = '#6e6a86',
		subtle = '#908caa',
		text = '#e0def4',
		love = '#eb6f92',
		gold = '#f6c177',
		rose = '#ebbcba',
		pine = '#31748f',
		foam = '#9ccfd8',
		iris = '#c4a7e7',
		highlight_low = '#21202e',
		highlight_med = '#403d52',
		highlight_high = '#524f67',
		ground = '#57949f',
		base2 = "#191919",
		pine_hard = "4a6474",
		none = 'NONE',
	},
	moon = {
		base = '#212121',
		surface = '#222222',
		overlay = '#45403b',
		muted = '#7a7267',
		subtle = '#a49689',
		--text = '#e8d4c0',
		text = '#d1bfae',
		love = '#d16969',
		gold = '#e8b276',
		rose = '#ce7b62',
		pine = '#556e84',
		foam = '#84c1c8',
		iris = '#db784d',
		highlight_low = '#2b2a29',
		highlight_med = '#363636',
		highlight_high = '#605953',
		pine_med = '#648ba2',
		text_doc = '#a49689',
		ground = '#57949f',
		base2 = "#1e1e1e",
		pine_hard = "4a6474",
		none = 'NONE',
	},
	dawn = {
		base = '#faf4ed',
		surface = '#fffaf3',
		overlay = '#f2e9e1',
		muted = '#9893a5',
		subtle = '#797593',
		text = '#575279',
		love = '#b4637a',
		gold = '#ea9d34',
		rose = '#d7827e',
		pine = '#286983',
		foam = '#56949f',
		iris = '#907aa9',
		highlight_low = '#f4ede8',
		highlight_med = '#dfdad9',
		highlight_high = '#cecacd',
		ground = '#57949f',
		base2 = "#e8d4c0",
		pine_hard = "4a6474",
		none = 'NONE',
	},
}

local palette = {}

if vim.o.background == 'light' then
	palette = variants.dawn
else
	palette = variants[(vim.g.propepine_variant == 'moon' and 'moon') or 'main']
end

return palette
