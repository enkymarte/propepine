local p = require('propepine.palette')

return {
	normal = {
		a = { bg = p.base, fg = p.text, gui = 'bold' },
		b = { bg = p.overlay, fg = p.pine_med },
		c = { bg = p.base2, fg = p.text },
	},
	insert = {
		a = { bg = p.gold, fg = p.base, gui = 'bold' },
		b = { bg = p.overlay, fg = p.gold },
		c = { bg = p.base2, fg = p.text },
	},
	visual = {
		a = { bg = p.foam, fg = p.base, gui = 'bold' },
		b = { bg = p.overlay, fg = p.foam },
		c = { bg = p.base2, fg = p.text },
	},
	replace = {
		a = { bg = p.pine_hard, fg = p.base, gui = 'bold' },
		b = { bg = p.overlay, fg = p.pine_hard },
		c = { bg = p.base2, fg = p.text },
	},
	command = {
		a = { bg = p.rose, fg = p.base, gui = 'bold' },
		b = { bg = p.overlay, fg = p.rose },
		c = { bg = p.base2, fg = p.text },
	},
	inactive = {
		a = { bg = p.base, fg = p.muted, gui = 'bold' },
		b = { bg = p.base, fg = p.muted },
		c = { bg = p.base, fg = p.muted },
	},
}
